<?php



namespace fjerbi\BlogBundle;

use fjerbi\BlogBundle\DependencyInjection\fjerbiBlogExtension;
use Symfony\Component\HttpKernel\Bundle\Bundle;


class BlogBundle extends Bundle
{
    public function getContainerExtension()
    {
        return new fjerbiBlogExtension();
    }
}
